import React from 'react';
import logo from './logo.svg';
import './App.css';
import Customer from './components/Customer'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableHead from '@material-ui/core/TableHead'
import TableFooter from '@material-ui/core/TableFooter'
import { TableRow, TableCell } from '@material-ui/core';
import Paper from '@material-ui/core/Paper'
import {withStyles} from '@material-ui/core/styles'

const customers = [
  {
   'id' : '001'
  ,'name' : '김성남'
  ,'birthday' : '910831'
  ,'gender' : '남자'
  ,'address' : '부산'
  ,'image' : 'https://placeimg.com/64/64/1'
},
{
  'id' : '002'
 ,'name' : '김성영'
 ,'birthday' : '931025'
 ,'gender' : '남자'
 ,'address' : '서울'
 ,'image' : 'https://placeimg.com/64/64/2'
},
{
  'id' : '003'
 ,'name' : '김개똥'
 ,'birthday' : '951221'
 ,'gender' : '남자'
 ,'address' : '제주'
 ,'image' : 'https://placeimg.com/64/64/3'
}
]

const styles = theme => (
  {
    root:{
      width: "100%",
      marginTop: theme.spacing.unit *3,
      overflowX: "auto"

    },
    table : {
      minWidth: 1080
    }
  }
)

function App(props) {
  const {classes} = props;
  return (
    <Paper className={classes.root}>
    <Table className={classes.table}>
      <TableHead>
        <TableRow>
          <TableCell>id</TableCell>
          <TableCell>profile</TableCell>
          <TableCell>이름</TableCell>
          <TableCell>생년월일</TableCell>
          <TableCell>성별</TableCell>
          <TableCell>주소</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {
          customers.map( c => {
            return(
              <Customer
                key={c.id}
                id={c.id}
                name={c.name}
                birthday={c.birthday}
                gender={c.gender}
                address={c.address}
                image={c.image}
                />
            )
          })
        }
      </TableBody>
    </Table>
    </Paper>
  );
}

export default withStyles(styles)(App);
